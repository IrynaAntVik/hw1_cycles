public class Hw1cycle3 {
    public void coren(int x) {
        if (x < 0) {
            System.out.println("Квадратный корень из отрицательного числа не существует");
        } else {
            int y = -1;
            do {
                y += 1;
            } while ((y + 1) * (y + 1) <= x);
            System.out.println(y + " - корень натурального числа " + x + " с точностью до целого");
        }
    }
}
