public class Hw1cycle6 {
    public int mirror(int z) {
        double y = 0;
        int i = 0;
        while (z / Math.pow(10, i) >= 1 || z / Math.pow(10, i) <= -1) {
            y = y * 10 + (Math.floor((Math.abs(z) / Math.pow(10, i)) % 10));
            i += 1;
        }
        if (z >= 0) {
            return  (int) y;
        } else {
            return  -(int) y;
        }
    }
}
