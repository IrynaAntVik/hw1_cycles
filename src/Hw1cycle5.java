public class Hw1cycle5 {
    public int sumcifr(int z) {
        int y = 0;
        int i = 0;
        do {
            y += (z / (int)Math.pow(10, i)) % 10;
            i += 1;
        }
        while (z / (int)Math.pow(10, i) > 1);
        return y;
    }
}
