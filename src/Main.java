public class Main {
    public static void main(String[] args) {
        init();
    }

    public static void init(){
        System.out.println("Задание 1:");
        Hw1cycle1 hw1cycle1 = new Hw1cycle1();
        System.out.println(hw1cycle1.sumChetnih() + " - cумма всех четных чисел от 1 до 99");
        System.out.println("Задание 2:");
        Hw1cycle2 hw1cycle2 = new Hw1cycle2();
        hw1cycle2.prime(173);
        hw1cycle2.prime(0);
        hw1cycle2.prime(175);
        hw1cycle2.prime(-35);
        System.out.println("Задание 3 методом последовательного подбора:");
        Hw1cycle3 hw1cycle3 = new Hw1cycle3();
        hw1cycle3.coren(-144);
        hw1cycle3.coren(0);
        hw1cycle3.coren(1);
        hw1cycle3.coren(121);
        hw1cycle3.coren(142);
        System.out.println("Задание 3 методом бинарного поиска:");
        Hw1cycle3bin hw1cycle3bin = new Hw1cycle3bin();
        hw1cycle3bin.corenbin(-144);
        hw1cycle3bin.corenbin(0);
        hw1cycle3bin.corenbin(1);
        hw1cycle3bin.corenbin(121);
        hw1cycle3bin.corenbin(142);
        System.out.println("Задание 4");
        Hw1cycle4 hw1cycle4 = new Hw1cycle4();
        hw1cycle4.factorial(-5);
        hw1cycle4.factorial(0);
        hw1cycle4.factorial(1);
        hw1cycle4.factorial(5);
        System.out.println("Задание 5");
        Hw1cycle5int hw1cycle5int = new Hw1cycle5int();
        Hw1cycle5 hw1cycle5 = new Hw1cycle5();
        System.out.println(hw1cycle5.sumcifr(hw1cycle5int.help(-4.5)) + " - cумма цифр числа");
        System.out.println(hw1cycle5.sumcifr(hw1cycle5int.help(0)) + " - cумма цифр числа");
        System.out.println(hw1cycle5.sumcifr(hw1cycle5int.help(0.5)) + " - cумма цифр числа");
        System.out.println(hw1cycle5.sumcifr(hw1cycle5int.help(600108.05)) + " - cумма цифр числа");
        Hw1cycle6 hw1cycle6 = new Hw1cycle6();
        System.out.println(hw1cycle6.mirror(0) + " - зеркальное отображение последовательности цифр числа");
        System.out.println(hw1cycle6.mirror(8) + " - зеркальное отображение последовательности цифр числа");
        System.out.println(hw1cycle6.mirror(74) + " - зеркальное отображение последовательности цифр числа");
        System.out.println(hw1cycle6.mirror(12004) + " - зеркальное отображение последовательности цифр числа");
        System.out.println(hw1cycle6.mirror(-9354) + " - зеркальное отображение последовательности цифр числа");
    }
}