public class Hw1cycle3bin {
    public void corenbin(int x) {
        int a = 0;
        int b = x;
        int res = 0;
        int c;
        if (x < 0) {
            System.out.println("Квадратный корень из отрицательного числа не существует");
        } else {
            if (x == 0 || x ==1) {
                System.out.println(x + " - корень натурального числа " + x +" с точностью до целого");
            } else {
                while (a < b) {
                    c = (int) Math.floor((a + b) / 2);
                    if ((c * c <= x) && ((c + 1) * (c + 1) > x)) {
                        res = c;
                        a = b;
                    } else {
                        if (c * c > x) {
                            b = c;
                        } else {
                            if (c * c < x) {
                                a = c;
                            }
                        }
                    }
                }
                System.out.println(res + " - корень натурального числа " + x + " с точностью до целого");
            }
        }
    }
}
