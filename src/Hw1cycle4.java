public class Hw1cycle4 {
    public void factorial(int x) {
        int res;
        if (x < 0) {
            System.out.println("Факториал отрицательного числа не существует");
        } else {
            if (x == 0) {
                res = 1;
            } else {
                res = 1;
                for (int i = 1; i <= x; i = i + 1) {
                    res *= i;
                }
            }
            System.out.println(res + " - факториал числа " + x);
        }
    }
}
