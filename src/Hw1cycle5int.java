public class Hw1cycle5int {
    public int help(double f) {
        double g = f;
        while (g != Math.floor(g)) {
            g *= 10;
        }
        int w = (int) Math.abs(g);
        return w;
    }
}
